from django.apps import AppConfig


class WhastappConfig(AppConfig):
    name = 'whastapp'
