from datetime import datetime, date
from dateutil.relativedelta import relativedelta
from nltk.tokenize import sent_tokenize, word_tokenize
from dateutil.parser import parse

# Funcion para formatear la fecha
def formater_date(_date):
	
	dt = parse(_date).strftime('%Y-%m-%d')
	return dt


#Funcion para incrementar fechas cada 3 meses
def amount_date(_date, _month):

	dt = parse(_date)

	date_before = date(dt.year, dt.month, dt.day)
	date_after = date(dt.year, dt.month, dt.day) + relativedelta(months = _month)
	return (date_before, date_after)