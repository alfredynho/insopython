import sys, time
from colorclass import Color

# Funcion para mostrar barra de progreso graficamente
def charger(count, total, status=''):
	bar_len = 25
	filled_len = int(round(bar_len * count / float(total)))

	percents = round(100.0 * count / float(total), 1)
	bar = Color('{green}#{/green}') * filled_len + '-' * (bar_len - filled_len)

	sys.stdout.write('[%s] %s%s >>> %s <<< \r'  % (bar, percents, '%', status))
	sys.stdout.flush()

# Funcion para la barra de progreso
def progress(end, message):
	count = 1
	while count < end:
		charger(count, end, message)
		time.sleep(0.5)
		count += 1



def print_background():
	print("\033[1;32;40m  \n")	

def print_fail(message):
    print('\x1b[1;31m' + message.strip() + '\x1b[0m' + '\n')

def print_pass(message):
    print('\x1b[1;32m' + message.strip() + '\x1b[0m' + '\n')

def print_warn(message):
    print('\033[0;37;41m' + message.strip() + '\x1b[0m' + '\n')

def print_info(message):
    print('\x1b[1;34m' + message.strip() + '\x1b[0m')

def print_bold(message):
    print('\x1b[1;37m' + message.strip() + '\x1b[0m' + '\n')

def print_success(message):
    print('\033[0;37;46m' + message.strip() + '\x1b[0m' + '\n')

def print_movement(message):
	print('\033[5;37;40m' + message.strip() + '\033[0;37;40m')    