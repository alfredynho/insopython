import json, os, xlsxwriter # Importacion de Json, os para ejecutar funciones del sistema y xlsxwriter para generar archivos Excel
from means.progressbar import progress # Agregar barra de progreso
from means.write import write_text, write_json_dates, linear_paragraph  , json_lineal , json_merge, json_matriz
from means.cleaner import count_bad_word # Importacion de la funcion para contar palabras malas
from means.extract import names_json_files # Importacion de la funcion para sacar todos los nombres de los archivos json


# Funcion para mandar a llamar a las funciones de:
# Escribir jsons , 
# limpiar, 
# generar matriz, 
# json de palabras malas cada 3 meses
# json de cantidad de palabras malas por conversacion 


def read_text(sw, route):
	# Si la opcion es 1 
	# Realiza el listado de archivos json
	# llamando alas funciones para su limpieza
	if sw == 1:
		json_files = names_json_files(route)
		for file in json_files:
			read = json.loads(open('data/'+route+'/'+str(file)).read())
			list_messages = read['messages']
			contactName = read['contactName']
			progress(5,"LIMPIANDO JSON - {}".format(file))
			write_text(list_messages, file, contactName)


	# Si la opcion es 2
	# Clasifica los archivos ya limpios cada 3 meses
	if sw == 2:
		json_files = names_json_files(route)

		for file in json_files:		
			read = json.loads(open('data/'+route+'/'+file).read())    			
			list_messages = read['messages']
			query_file_name = read['name_file']
			progress(5,"CLASIFICANDO JSON - {}".format(file))
			write_json_dates(list_messages, file, query_file_name)

	# si la opcion es 3
	# Contabiliza la cantidad de palabras malas por conversacion
	if sw == 3:
		json_files = names_json_files(route)
		for file in json_files:		
			read = json.loads(open('data/'+route+'/'+str(file)).read())    			
			list_messages = read['messages']
			progress(5,"{}".format(file))
			linear_paragraph(list_messages, file)


	# Si la opcion es 4
	# Lleva todas las conversaciones a una sola linea por conversacion a un solo json
	if sw == 4:
		json_files = names_json_files(route)
		_chain = ""

		for file in json_files:		
			read = json.loads(open('data/'+route+'/'+str(file)).read())    			
			list_messages = read['messages']
			progress(5,"{}".format(file))
			chain = json_merge(list_messages, file)
			for x in chain:			
				_chain = _chain +' '+ x
		json_lineal(_chain,"matriz.json") 


	# Si la opcion es 5
	# Genera la matriz

	if sw == 5:
		list_matriz = []
		_row1 = 1
		_col1 = 1

		# Asignamos una nombre al documento
		workbook = xlsxwriter.Workbook('jsonmatriz.xlsx')
		worksheet = workbook.add_worksheet()
		bold = workbook.add_format({'bold': True})

		worksheet.set_column('A:B', 30)

		# Agrega en las cabeceras el titulo del documento
		worksheet.write('A1', 'Generando Matriz', bold)
		worksheet.write('B1', 'Matriz de Palabras', bold)


		with open('data/jsonmatriz/̣' + 'matriz.json', "r+") as jsonFile:
			data = json.load(jsonFile)
			for x in data['messages']:
				for d in x['message']:
					list_matriz.append(d)

		for word in list_matriz:

			worksheet.write_string(_row1, _col1, word)
			_col1 += 1
		worksheet.write_string(_row1, _col1,"Clasificador")


		json_files = names_json_files(route)

		_row = 2

		t1 = 2
		t2 = 0
		at1 = 2

		cc = 1
		_cc = 1

		for file in json_files:
			t1 = at1
			worksheet.write_string(t1, t2, file)
			t1 +=1
			at1 = t1
			print("Conversacion  >> ", file)
			read = json.loads(open('data/'+route+'/'+str(file)).read()) 			
			list_messages = read['linear_chain']
			classification = str(read['classification'])
			progress(5,"{}".format(file))

			count = 1
			for word in list_matriz:
				import random
				# dd = random.randint(0, 5)				
				cont = count_bad_word(word,list_messages)
				worksheet.write_string(_row, cc, str(cont))
				count +=1
				cc +=1
				_cc = cc

			_dd = random.randint(0, 1)	
			worksheet.write_string(_row, cc, classification)

			cc = 1
			_row +=1

		workbook.close()
