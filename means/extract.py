import os # Importacion de OS para ejecutar comandos del sistema

# Funcion para obtener todos los archivos en formato json segun la ruta pasada
def names_json_files(route):

	lstFiles = []
 
	lstDir = os.walk('data/'+route)

	for root, dirs, files in lstDir:
		for file in files:
			(name_file, extension) = os.path.splitext(file)
			if(extension == ".json"):
				lstFiles.append(name_file+extension)

	return lstFiles