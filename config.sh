#!/bin/bash

################################################################
##
##   Script for Create, drop database > MYSQL/POSTGRESQL Version 1.0
##   Last Update: 17 Marzo 2019
##   actualization : 19 Noviembre 2020
################################################################

RED="\033[1;31m"
GREEN="\033[1;32m"
NOCOLOR="\033[0m"
BLUE="\[\033[0;34m\]"
UBlue="\[\033[4;34m\]"
On_Blue="\[\033[44m\]"
IYellow="\[\033[0;93m\]"
BIYellow="\[\033[1;93m\]"


config_db_postgres(){
   printf 'Nombre del proyecto sin espacios: '
   read APP
   _DB=${APP,,}

   # SET CONFIG POSTGRES
   DATABASE="${_DB}_app"   # DATABASE NAME
   USER="${_DB}_user"   # DATABASE USERNAME
   PSQLPASSDATABASE="XXXYYYZZZ"  # DATABASE PASSWORD
   PORT_POSTGRES="5432" # PORT POSTGRES
   PGSQL="pgsql" # TYPE DATABASE

   sudo -u postgres psql -c "DROP DATABASE $DATABASE"
   sudo -u postgres psql -c "DROP USER $USER"
   sudo -u postgres psql -c "CREATE USER $USER WITH PASSWORD '$PSQLPASSDATABASE';"
   sudo -u postgres psql -c "CREATE DATABASE $DATABASE WITH OWNER $USER"
   echo -e "${GREEN} Base de datos creado Exitosamente ... ${GREEN}"

}


config_db_mariadb(){
   # SET CONFIG MYSQL
   PORT_MYSQL="3306" # PORT MYSQL
   MYSQLUSER="root" #USER MYSQL DEFAULT   
   PSQLPASSSYSTEM="5dv4rrgq8au7"  # SYSTEM PASSWORD

   printf 'Nombre del proyecto sin espacios: '
   read APP
   _DB=${APP,,}

   mysql -u root -p$PSQLPASSSYSTEM -e "DROP DATABASE $_DB;" 2> /dev/null
   mysql -u root -p$PSQLPASSSYSTEM -e "CREATE DATABASE $_DB;" 2> /dev/null

   echo -e "${GREEN} Base de datos creado Exitosamente ... ${GREEN}"

}


while :
do
   printf 'Con que base de datos trabajar?: POSTGRES,MARIADB: '
   read OPTION

   _DATABASES=${OPTION,,}

   case $_DATABASES in
      postgres)
         config_db_postgres
         sleep 3
         break
         ;;
      mariadb)
         config_db_mariadb
         sleep 3
         break
         ;;
      *)
         clear
         echo "No ingresaste ninguna opcion ctrl+c para salir"
         ;;
   esac
done

