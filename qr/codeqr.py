import qrcode  
import pandas
import os
import shutil

from means.progressbar import print_background, print_fail ,print_pass, print_warn, print_info, print_bold, print_success, print_movement
from means.progressbar import progress # Agregar barra de progreso

#CODE EXAMPLE QR = COVID19/0V1/INSO-G-CSM/NAME/20-04-2020/CV001

list_participant = pandas.read_csv('qr/cplagui2septiembre.csv')

def verify(certificate):
      if certificate==1:
            return "Aprobo_Curso"
      if certificate==2:
            return "Docente"
      else:
            return "Participante"

def is_nan(_value):
      if _value=="nan":
          return " "
      else:
          return _value  

def generate_qr():
    _verc = '1'
    _name_c = 'CURSO TEORICO-PRACTICO SOBRE "USO, MANEJO DE APLICACION DE PLAGUICIDAS DE USO DOMESTICO"'

# INSO/CURSO TEORICO-PRACTICO SOBRE "USO, MANEJO DE APLICACION DE PLAGUICIDAS DE USO DOMESTICO"/REALIZADO DE 27 AL 30 DE JULIO/INSO-G-CSM/ALAIN DUBER CASIAS MARQUEZ/APROBADO/22846/CERTIFICADO3
    _fecha = 'REALIZADO DE 14 AL 17 DE SEPTIEMBRE del 2020' 
    _gestion = 'CSM'

    #shutil.rmtree('CERTIFI')
    #os.mkdir('CERTIFI')
    _cont = 1

    for x , lp in list_participant.iterrows():
        progress(2,"GENERANDO QR - {}".format(lp['Nombres']))
        print_background()
      #   imagen = qrcode.make(_name_c+'/'+'INSO-G-'+_gestion+'/'+str(lp['Nombres'])+' '+str(lp['Paterno'])+' '+str(lp['Materno'])+'/'+_fecha+'/'+'CV'+str(_cont))
        imagen = qrcode.make(_name_c+'/'+'INSO-G-'+_gestion+'/'+str(lp['Nombres'])+'/'+_fecha+'/'+str(lp['Estado'])+'/'+str(lp['Factura'])+'CV'+str(_cont))
        #imagen = qrcode.make(_name_c+'/'+_gestion+'/'+str(lp['Nombres'])+' '+str(lp['Paterno'])+' '+str(lp['Materno'])+'/'+_fecha+'/'+'CV'+str(_cont))

        archivo_imagen = open('2PLAGUIQR/'+str(lp['Nombres'])+'.png', 'wb')
        _cont+=1
        imagen.save(archivo_imagen)
        archivo_imagen.close()


    # shutil.rmtree('CERTIFI')
    # os.mkdir('CERTIFI')
    # archivo_imagen = open('CERTIFI/'+str(_cont)+'-'+str(lp['Nombres'])+' '+str(lp['Paterno'])+' '+str(lp['Materno'])+'-'+str(verify(lp['Certificado']))+'.png', 'wb')
    # _cont+=1
    # imagen.save(archivo_imagen)
    # archivo_imagen.close()


    #CEMTIC/CURSO ADMINISTRACIÓN Y GESTIÓN DE LA PLATAFORMA MOODLE/CV002/HUGO MAMANI BONIFACIO/MAYO 2020