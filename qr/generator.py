import qrcode  
import pandas
import os
import shutil
import csv
from means.progressbar import print_background, print_fail ,print_pass, print_warn, print_info, print_bold, print_success, print_movement
from means.progressbar import progress # Agregar barra de progreso
from time import sleep
from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
import socket
from selenium import webdriver

from webdriver_manager.chrome import ChromeDriverManager

from selenium import webdriver

from webdriver_manager.chrome import ChromeDriverManager

#CODE EXAMPLE QR = COVID19/0V1/INSO-G-CSM/NAME/20-04-2020/CV001

list_participant = pandas.read_csv('qr/numeros/participants.csv')

def verify(certificate):
      if certificate==1:
            return "Aprobo_Curso"
      if certificate==2:
            return "Docente"
      else:
            return "Participante"

def is_nan(_value):
      if _value=="nan":
          return " "
      else:
          return _value  

def generate_qr():
    _verc = '1'
    _name_c = 'PSICOLOGIA EN ATENCION DE LA SALUD'
    _fecha = '5-2020'
    _gestion = 'CSM'

    _cont = 1

    shutil.rmtree('PSICOV2')
    os.mkdir('PSICOV2')   
    list_num = []

    for x , lp in list_participant.iterrows():
        progress(2,"celular - {}".format(lp['phone']))
        print(lp['phone'])
        if lp['phone'] in list_num:
            print("esta")
        else:
            list_num.append(lp['phone'])
            print([list_num])

        print_background()

    print(list_num)	
        
        #   imagen = qrcode.make(_name_c+'/'+'INSO-G-'+_gestion+'/'+str(lp['Nombres'])+' '+str(lp['Paterno'])+' '+str(lp['Materno'])+'/'+_fecha+'/'+'CV2'+str(_cont)+'/'+str(verify(lp['Certificado'])))
        #   archivo_imagen = open('PSICOV2/'+str(_cont)+'-'+str(lp['Nombres'])+' '+str(lp['Paterno'])+' '+str(lp['Materno'])+'-'+str(verify(lp['Certificado']))+'.png', 'wb')
        #   _cont+=1
        #   imagen.save(archivo_imagen)
        #   archivo_imagen.close() 


    # mensaje que quieres enviar
    #message_text = 'CEMTIC.'

    message_text = '*INSTITUTO NACIONAL DE SALUD OCUPACIONAL VIDEOCONFERENCIA EL LIDER DE SEGURIDAD Y SALUD OCUPACIONAL DURANTE LA PANDEMIA POR COVID 19* \n  *EXPOSITOR:* Dr. Juan Carlos Balladares \n *Estimado (a) participante:* La videoconferencia se desarrollara el día sabado 30 de mayo a las 19:00 pm . \n   Puede realizar su inscripción  en el siguiente formulario. *LINK DEL FORMULARIO:* https://inso.digital/curso/liderazgo-salud \n Reciba un cordial saludo *El equipo del Centro de Capacitación del Instituto Nacional de Salud Ocupacional.*'

    no_of_message = 1  # no. de tiempo desea que el mensaje sea enviado
    # lista de números de teléfono puede ser de cualquier longitud
    # Puedes agregar a la lista mas de un numero ejem  [573024508559,num2,num3,num4]
    #moblie_no_list = [5526579800,936789362,59172911957,59176537186,59162326996,59175231559,59177703259,962905120,59175303523,59178643372,59165507320,59176746434,59160501121,59177709130,59177572252,59177774375,3006881950,1720260652,59172572993,593995519384,59171496919,59175804308,59160621324,59167388460,59177238023]

    moblie_no_list = [
        59171945314,
        59176509898,
        59170503407,
        59177746455,
        59160533877,
        59179523475,
        59173229320,
        59176590763,
        59161158412,
        59160511497,
        59179136591,
        59161226831,
        59179142624,
        59172920377,
        59170589790,
        59169923442,
        59170692698,
        59175853207,
        59168110661,
        59170581075,
        59175228360,
        59160622456,
        59169906614,
        59177566108,
        59172018876,
        59173056952,
        59177502916,
        59177536566,
        59171965044,
        59173422692,
        59170690863,
        59173064074,
        59169791769,
        59172003445,
        59176295892,
        59160617483,
        59170503407,
        59160533877,
        59169791769,
        59177245135,
        59169850092,
        59177236359,
        59179151127,
        59160127106,
        59172528240,
        59170526381,
        59167040003,
        59169864488,
        59179107005,
        5916953831,
        59178959636,
        59173299584,
        59172575480,
        59171258280,
        59178945353,
        59160622456,
        59167137694,
        59177502916,
        59176179476,
        59169906614,
        59169801418,
        5915968531,
        59165565525,
        59173026112,
        59172557567,
        59174057747,
        59167327417,
        59173015734,
        59173213405,
        59179159566,
        59170549487,
]

    def element_presence(by, xpath, time):
        element_present = EC.presence_of_element_located((By.XPATH, xpath))
        WebDriverWait(driver, time).until(element_present)


    def is_connected():
        try:
            # conectarse al host: nos dice si el host es en realidad
            # accesible
            socket.create_connection(("www.google.com", 80))
            return True
        except:
            is_connected()


    driver = webdriver.Chrome(ChromeDriverManager().install())
    driver.get("http://web.whatsapp.com")
    sleep(10)  # esperar tiempo para escanear el código en segundo

    def send_whatsapp_msg(phone_no, text):
        driver.get(
            "https://web.whatsapp.com/send?phone={}&source=&data=#".format(phone_no))
        try:
            driver.switch_to_alert().accept()


        except Exception as e:
            pass

        try:
            element_presence(
                By.XPATH, '//*[@id="main"]/footer/div[1]/div[2]/div/div[2]', 30)
            txt_box = driver.find_element(
                By.XPATH, '//*[@id="main"]/footer/div[1]/div[2]/div/div[2]')


            global no_of_message
            for x in range(no_of_message):
                txt_box.send_keys(text)
                txt_box.send_keys("\n")# el enter para enviar 

        except Exception as e:
            print("numero no valido :"+str(phone_no))


    for moblie_no_list in list_num:
        try:
            send_whatsapp_msg(moblie_no_list, message_text)
        except Exception as e:
            sleep(10)
            is_connected()

