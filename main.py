#!/usr/bin/python3
# SCRIPT PARA EL CENTRO DE CAPACITACION DEL INSO GESTION 2020

from means.read import read_text # Importando la funcion para leer los jsons o xls que se agregaran de los participantes a los cursos
from prettytable import PrettyTable # Libreria para formatear el menu de opciones
import os # Importando OS para ejecutar comandos linux
from means.extract import names_json_files # Importacion de la funcion para sacar todos los nombres de los archivos json o xls listas de los cursos
from qr.codeqr import generate_qr
from certificados.certify import generate_certify
from certificados.certify2 import generate_certify2

# Formateando Menu
x = PrettyTable(["Opciones", "Comandos"])

# Cargando Opciones al Menu

x.add_row(["Generar QR ", 0])
x.add_row(["Generar PDF de cursos", 1])
x.add_row(["Enviar PDF a numeros", 2])
x.add_row(["Enviar mensajes masivos por Whastapp", 4])
x.add_row(["Salir", 5])

# Creando Menu de Opciones
def menu():
    # Limpiando la terminal
    os.system("clear")
    # Mostrando Menu de Opciones
    print('\033[1;32;40m' + '\033[1m' + x.get_string(title='>>>>>> INSO GENERADOR DE QR Y MENSAJES MASIVOS POR WHASTAPP <<<<<<' + '\033[1;32;40m'))

while True: # Siempre y cuando la condicion sea True

    menu()
    option = input("Ingrese su opcion ")

    # Opcion 0 Realiza el cargado de los archivos json
    if option == "0":
        print("")
        cont = 0

        generate_qr()

        print("\n")
        input(" >>> Se termino de generar los QR \n... Presiona Enter para continuar ")        

    if option == "1":
        print("")

        generate_certify2()

        print("\n")
        input(" >>> Se realizo el generado de certificados \n... Presiona Enter para continuar ")        
        
    # Opcion 2 Realiza la clasificacion de Archivos cada 3 meses
    elif option == "2":
        print("")
        read_text(2,'jsondb')

        print("\n")
        input(" >>> Calificacion y palabras malas cada 3 Meses \n... Presiona Enter para continuar ")        

    # Opcion para llevar todas las conversaciones a una sola linea
    elif option == "3":
        print("")
        read_text(3,'jsondb')

        print("\n")
        input(" >>> Cantidad de palabras Malas por Mes \n... Presiona Enter para continuar ")                

    # Opcion para sacar cantidad de palabras malas por conversacion y Generar Matriz
    elif option == "4":
        print("")
        # para llevar todas las conversaciones a una linea
        read_text(4,'jsonqualification')
        # funcion para generar matriz a partir de un solo archivo el matriz.json 
        read_text(5,'jsondb')

        print("\n")
        input(" >>> Se termino de generar la Matriz  \n... Presiona Enter para continuar ")                

    # Opcion para terminar el flujo
    elif option == "5":
        print("Bye ...")
        break

    else: # Mensaje por default
        print("")
        input("No ingresaste ninguna opcion \n presiona enter para continuar ")