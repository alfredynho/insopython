
# from reportlab.pdfgen import canvas
 
# c = canvas.Canvas("fileName.pdf")
# c.drawString(100,750,"This is my first PDF file in Reportlab!")
# c.save()

from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
from reportlab.lib.pagesizes import landscape
from reportlab.platypus import Image
import pandas

import csv
import os

from csv import reader

import codecs

# data_file = "qr/tallerlider.csv"
#Celular,Nombres,Paterno,Materno,Cedula

pdfFolder = 'certificados/pdfFolderTallerLider/'
if not os.path.exists(pdfFolder):
	os.makedirs(pdfFolder)

def generate_certify():

	#list_participant = pandas.read_csv('qr/certificados.csv')
	listado = reader(open('certificados/tallerlider.csv','r'))

	#listado = reader(open('qr/certificados.csv','r'))
	cont = 1
	for row in listado:
		# celular = row[0]
		nombres = row[0]
		paterno = row[1]
		materno = row[2]
		pdf_file_name = pdfFolder + nombres + '.pdf'
		print(nombres,materno,paterno)
		cont +=1
		generate_certificate(nombres,paterno,materno, pdf_file_name)

def generate_certificate(nombres,paterno,materno, pdf_file_name):
	c = canvas.Canvas(pdf_file_name)
	#c = canvas.Canvas(pdf_file_name, pagesize=landscape(letter))

	# Add image border template`
	image_path = 'certificados/tallerlider.png'
	c.drawImage(image_path, 0, 0, width=600, height=845)

	try:

		# Add image qr code /home/alfredynho/Documents/proyectos/devpy/insopython/CERTIFI/591732124.png
		image_path = '/home/alfredynho/Documents/proyectos/devpy/insopython/LIDERQR/{nombres}.png'.format(nombres=nombres)
		c.drawImage(image_path, 270,230, width=90, height=90)

		# Header Text
		c.setFont("Helvetica-Bold", 24, leading=None)
		c.drawCentredString(300, 490, nombres+' '+paterno+ ' ' +materno)

		# Body Text
		# c.setFont("Helvetica", 24, leading=None)
		# c.drawCentredString(400, 420, 'This certifies that')

		# Participant Name Text
		# c.setFont("Helvetica-Bold", 30, leading=None)
		# c.drawCentredString(400, 340, name)

		# Draw line under the name - line(x,y, x,y)
		# c.line(200,320, 600,320)

		# More body Text ...
		# c.setFont("Helvetica", 24, leading=None)
		# c.drawCentredString(400, 260, 'Has completed the ' + course_title + ' programming training course at')

		# School Name Text
		# c.setFont("Helvetica-Bold", 24, leading=None)
		# c.drawCentredString(400, 180, 'ABC Computer Institute')

		c.showPage()
		c.save()



	except:
		pass

#main(data_file)























