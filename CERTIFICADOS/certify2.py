
# from reportlab.pdfgen import canvas
 
# c = canvas.Canvas("fileName.pdf")
# c.drawString(100,750,"This is my first PDF file in Reportlab!")
# c.save()

from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
from reportlab.lib.pagesizes import landscape
from reportlab.platypus import Image
import pandas

import csv
import os

from csv import reader

import codecs

# data_file = "qr/tallerlider.csv"
#Celular,Nombres,Paterno,Materno,Cedula

pdfFolder = 'certificados/pdfCursoPlagui/'

if not os.path.exists(pdfFolder):
	os.makedirs(pdfFolder)

def generate_certify2():

	#list_participant = pandas.read_csv('qr/certificados.csv')
	listado = reader(open('certificados/cplagui2septiembre.csv','r'))

	#listado = reader(open('qr/certificados.csv','r'))
	cont = 1
	for row in listado:
		nombres = row[0]
		# paterno = row[1]
		# materno = row[2]
		# celular = row[3]
		pdf_file_name = pdfFolder + nombres + '.pdf'
		print(nombres)
		cont +=1
		generate_certificate(nombres , pdf_file_name)

def generate_certificate(nombres, pdf_file_name):
    c = canvas.Canvas(pdf_file_name, pagesize=landscape(letter))
    image_path = '/home/alfredynho/Documents/projects/devpy/insopython/certificados/PLANTILLAS CERTIFICADO CURSO INSO/APROBADOPLAGUI.jpg'
    # c.drawImage(image_path, 0, 0, width=600, height=845)
    c.drawImage(image_path, 0, 0, width=800, height=590)
    # c.drawImage(image_path, 0, 0, width=810, height=620)
    image_path = '/home/alfredynho/Documents/projects/devpy/insopython/2PLAGUIQR/{nombres}.png'.format(nombres=nombres)
    #c.drawImage(image_path, 270,230, width=90, height=90) #sin convenio
    c.drawImage(image_path, 390,12, width=70, height=70) #convenio
    c.setFont("Times-Roman", 24, leading=None)
    c.drawCentredString(380, 300, nombres)
    c.showPage()
    c.save()


#Curso de Google
# def generate_certificate(nombres, pdf_file_name):
#     c = canvas.Canvas(pdf_file_name, pagesize=landscape(letter))
#     image_path = '/home/alfredynho/Documents/projects/devpy/insopython/certificados/PLANTILLAS CERTIFICADO CURSO INSO/GOOGLE TEMPLATE.jpg'
#     # c.drawImage(image_path, 0, 0, width=600, height=845)
#     c.drawImage(image_path, 0, 0, width=800, height=590)
#     # c.drawImage(image_path, 0, 0, width=810, height=620)
#     image_path = '/home/alfredynho/Documents/projects/devpy/insopython/GOOGLEQR/{nombres}.png'.format(nombres=nombres)
#     #c.drawImage(image_path, 270,230, width=90, height=90) #sin convenio
#     c.drawImage(image_path, 340,12, width=70, height=70) #convenio
#     c.setFont("Times-Roman", 24, leading=None)
#     c.drawCentredString(410, 300, nombres)
#     c.showPage()
#     c.save()























