import pandas
df = pandas.read_csv('covid1.csv')

def verify(certificate):
      if certificate==1:
            return "Aprobo_Curso"
      if certificate==2:
            return "Docente"
      else:
            return "Participante"

def is_nan(_value):
      if _value=="nan":
          return " "
      else:
          return _value  

for l ,r in df.iterrows():
    print(r['Nombres'],r['Paterno'], is_nan(r['Materno']), verify(r['Certificado']))