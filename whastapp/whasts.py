from time import sleep
from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
import socket

from selenium import webdriver

from webdriver_manager.chrome import ChromeDriverManager

message_text ='*INSTITUTO NACIONAL DE SALUD OCUPACIONAL* \n *PROGRAMA CURSO USO MANEJO Y APLICACIÓN DE PLAGUICIDAS DE USO DOMÉSTICO*'

no_of_message = 1

moblie_no_list = [
            59173612686,
    ]

def element_presence(by, xpath, time):
    element_present = EC.presence_of_element_located((By.XPATH, xpath))
    WebDriverWait(driver, time).until(element_present)

def is_connected():
    try:
        socket.create_connection(("www.google.com", 80))
        return True
    except:
        is_connected()


driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get("http://web.whatsapp.com")
sleep(10)

def send_whatsapp_msg(phone_no, text):
    driver.get(
        "https://web.whatsapp.com/send?phone={}&source=&data=#".format(phone_no))
    try:
        driver.switch_to_alert().accept()


    except Exception as e:
        pass

    try:

        element_presence(By.XPATH, '//*[@id="main"]/footer/div[1]/div[2]/div/div[2]', 30)
        txt_box = driver.find_element(By.XPATH, '//*[@id="main"]/footer/div[1]/div[2]/div/div[2]')
        
        global no_of_message
        for x in range(no_of_message):
            txt_box.send_keys(text)
            txt_box.send_keys("\n") 
        sleep(3)
    
        filepath = '/home/alfredynho/Documents/projects/devpy/whastapp/PLAGUI SANTA CRUZ.pdf'

        attachment_box = driver.find_element_by_xpath('//div[@title = "Adjuntar"]')
        attachment_box.click()
        image_box = driver.find_element_by_xpath('//input[@accept="image/*,video/mp4,video/3gpp,video/quicktime"]')
        image_box.send_keys(filepath)
        sleep(3)
        send_button = driver.find_element_by_xpath('//span[@data-icon="send"]')
        sleep(2)
        send_button.click()
        sleep(4)

    except Exception as e:
        print("numero no valido :"+str(phone_no))


for moblie_no in moblie_no_list:
    try:
        send_whatsapp_msg(moblie_no, message_text)
    except Exception as e:
        sleep(10)
        is_connected()
